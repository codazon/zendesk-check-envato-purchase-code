// Initialise Apps framework client. See also:
// https://developer.zendesk.com/apps/docs/developer-guide/getting_started
var client = ZAFClient.init();
client.invoke('resize', { width: '100%', height: '175px' });
client.metadata().then(function(metadata) {
  var fieldid = metadata.settings.fieldid;
  var customField = 'ticket.customField:custom_field_' + fieldid;
  var endpoint = metadata.settings.endpoint;
  client.get(customField).then(
    function(data) {
      var purchase_id = data[customField];
      requestPurchaseInfo(client, endpoint, purchase_id);
    }
  );
});


function requestPurchaseInfo(client, endpoint, purchase_id) {
  var settings = {
    url: endpoint+'&code='+purchase_id,
    type:'GET',
    dataType: 'json',
  };

client.request(settings).then(
    function(data) {
      if(data.error){
        showError(data);
      }else{
        showInfo(data);
      }
    }
  );
  }

function showInfo(data) {
  var requester_data = {
    'item_name': data.item.name,
    'buyer': data.buyer,
    'purchase_count': data.purchase_count,
    'sold_at': formatDate(data.sold_at),
    'supported_until': formatDate(data.supported_until),
    'valid_class': isValid(data.supported_until),
  };

  var source = $("#requester-template").html();
  var template = Handlebars.compile(source);
  var html = template(requester_data);
  $("#content").html(html);
}
function showError(response) {
  var error_data = {
    'status': response.error,
    'statusText': response.description
  };
  var source = $("#error-template").html();
  var template = Handlebars.compile(source);
  var html = template(error_data);
  $("#content").html(html);
}
function formatDate(date) {
  var cdate = new Date(date);
  var options = {
    year: "numeric",
    month: "short",
    day: "numeric"
  };
  date = cdate.toLocaleDateString("en-us", options);
  return date;
}

function isValid(supported_until){
  var d = new Date();
  var n = d.getTime();
  var sdate = new Date(supported_until);
  var sn = sdate.getTime();
  if(sn - n > 0){
    return 'valid';
  }else{
    return 'expired';
  }
}